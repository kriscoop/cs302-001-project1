#include "CLL.h" 

CLL :: CLL(): rear(nullptr)
{
}

CLL :: CLL(ActorNode * & aNode): rear(nullptr)
{
//	ActorNode * newNode = new ActorNode(aNode);
	insertActor(aNode);
}

CLL :: CLL(const ActorNode & actor): rear(nullptr)
{
	ActorNode * newNode = new ActorNode(actor);
	insertActor(newNode);
}

CLL :: ~CLL()
{
	destroy();
}

int CLL :: retrieveActor(const int & num, ActorNode * & aNode)
{
	if(!rear)
		return -1;
	if(num <= 0)
		return -2;
	else
	{
		int index = 0;
		if(index == num-1)
		{
			aNode = rear->getNext();
			return 1;
		}
		else
		{
			index++;
			return retrieveActor(rear->getNext()->getNext(), num, index, aNode);
		}
	}
}

int CLL :: retrieveActor(ActorNode * & current, const int & num, int & index, ActorNode * & aNode)
{
	if(current != rear)
	{
		if(num-1 == index)
		{
			aNode = current;
			return 1;
		}
		else
		{
			index++;
			return retrieveActor(current->getNext(), num, index, aNode);
		}
	}
	else
	{
		if(num-1 == index)
		{
			aNode = current;
			return 1;
		}
		else
			return 0;
	}
}

int CLL :: insertActor(ActorNode * & aNode)
{
	if(!rear)
	{
		ActorNode * newNode = new ActorNode(aNode);
		rear = newNode;
		rear->setNext(newNode);
		delete aNode;
		aNode = nullptr;
		return -1;
	}
	else
	{
		if(*aNode < *rear->getNext())
		{
			ActorNode * newNode = new ActorNode(aNode);
			newNode->setNext(rear->getNext());
			rear->setNext(newNode);
			delete aNode;
			aNode = nullptr;
			return 1;
		}
		else
			return insertActor(rear->getNext(), aNode);
	}
}

int CLL :: insertActor(ActorNode * & current, ActorNode *& aNode)
{
	if(current != rear)
	{
		if(*aNode < *current->getNext())	
		{
			ActorNode * newNode = new ActorNode(aNode);
			newNode->setNext(current->getNext());
			current->setNext(newNode);
			delete aNode;
			aNode = nullptr;
			return 1;
		}
		else
			return insertActor(current->getNext(), aNode);	
	}
	else
	{
		ActorNode * newNode = new ActorNode(aNode);
		newNode->setNext(current->getNext());
		current->setNext(newNode);
		rear = newNode;
		delete aNode;
		aNode = nullptr;
		return 1;
	}	
}

int CLL :: display() const
{
	if(!rear)
		return 0;
	else
	{
		cout << "ACTORS:" << endl;
		int index = 0;
		cout << index +1 << "." <<  endl; 
		(rear->getNext())->display();
		cout << endl;
		index++;
		display((rear->getNext())->getNext(), index);
	}
	return 1;
}

void CLL :: display(ActorNode *& current, int & index) const
{
	if(current != rear->getNext())
	{
		cout << index +1 << "." <<  endl; 
		current->display();
		cout << endl;
		index++;
		display(current->getNext(), index);
	}
}

int CLL :: removeActor(const int & num)
{
	if(!rear)
		return -1;
	if(num <= 0)
		return -2;
	else
	{
		int index = 0;
		if(num-1 == index)
		{
			if(rear == rear->getNext())
			{
				delete rear;
				rear = nullptr;
			}
			else
			{
				ActorNode * deletion = rear->getNext();
				rear->setNext((rear->getNext())->getNext());
				delete deletion;
			}
			return 1;
		}
		else
		{
			index++;
			return removeActor((rear->getNext())->getNext(), rear->getNext(),  num, index);
		}
	}
}

int CLL :: removeActor(ActorNode * & current, ActorNode * & prev,  const int & num, int & index)
{
	if(current != rear)
	{
		if(num-1 == index)
		{
			ActorNode * deletion = current;
			prev->setNext(current->getNext());
			delete deletion;
			return 1;
		}
		else
		{
			index++;
			return removeActor(current->getNext(), current, num, index);
		}
	}
	else
	{
		if(num-1 == index)
		{
			ActorNode * deletion = current;
			prev->setNext(current->getNext());
			delete deletion;
			rear = prev;
			return 1;
		}
		else
			return 0;
	}
}

int CLL :: removeAll()
{
	int result = destroy();
	return result;
}

int CLL :: destroy()
{
	if(rear)
	{
		destroy(rear->getNext());
		delete rear;
		rear = nullptr;
		return 1;
	}
	else
		return 0;
}

void CLL :: destroy(ActorNode * & current)
{
	if(current != rear)
	{
		destroy(current->getNext());
		delete current;
	}
}

CLL :: CLL(const CLL & to_copy): rear(nullptr)
{
	if(to_copy.rear)
	{
		ActorNode * current = to_copy.rear;
		ActorNode * newNode = new ActorNode(*current);
		rear = newNode;
		newNode->setNext(copy(to_copy.rear->getNext(), current));	
	}
}

CLL & CLL ::operator=(const CLL & to_copy)
{
	if(this == &to_copy)
		return *this;
	
	if(rear)
		destroy();

	if(to_copy.rear)
	{
		ActorNode * current = to_copy.rear;
		ActorNode * newNode = new ActorNode(*current);
		rear = newNode;
		newNode->setNext(copy(to_copy.rear->getNext(), current));	
	}
	return *this;
}

ActorNode *& CLL :: copy(ActorNode *& current, ActorNode *& tail)
{
	if(current != tail)
	{
		ActorNode * newNode = new ActorNode(*current);
		newNode->setNext(copy(current->getNext(), tail));
		return newNode;
	}
	else
		return rear;
}

istream & operator>>(istream & stream, CLL & list)
{
	if(stream)
	{
		if(list.rear)
			list.destroy();

		ActorNode * actor = new ActorNode(stream);
		while(!stream.eof())
		{
			list.insertActor(actor);
			actor = new ActorNode(stream);
		}
	
		delete actor;
	}
	return stream;	
}

ostream & operator << (ostream & stream, CLL & list)
{
	if(stream)
		if(list.rear)
			list.printToFile(list.rear->getNext(), stream);
	return stream;	
}

int CLL :: printToFile(ActorNode *& current, ostream & stream) const
{
	if(current != rear)
	{
		stream << current;
		printToFile(current->getNext(), stream);
	}
	else
		stream << current;
	return 1;
}



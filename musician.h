/*
Name: Kristian Cooper
Class#: CS-302-001
Project1
musician.h
This class is a derived class from the person class. It will have additional methods that allow user interaction such as purchasing a ticket, calculating concert revenue, and checking if the show is sold out (per ticket type).
*/

#ifndef MUSICIAN_H
#define MUSICIAN_H

#include "person.h"
#include <vector>

class Musician : public Person
{
	friend istream& operator>> (istream& stream, Musician & musician);
	friend ostream& operator<< (ostream& stream, Musician & musician);

public:
	Musician();
	Musician(istream & input);
	Musician(const string & aName, const int & section, const int & booth, const int & numOptions, const vector<string> & descrip, const vector<int> & cap, const vector<double> price);
	~Musician();

	int soldOut(const int & type) const;
	int purchaseTicket(const int & quantity, const int & type);
	int calculateRev() const; 
	int increaseCap(const int & capacity, const int & type);
	int updateOptions(const int & options, vector<string> tier, vector<double> price, vector<int> cap);

	int display() const;
	int displayPrices() const;

protected:
	
	int options;
	vector<string> optionDescrip;
	vector<int> capacity;
	vector<double> prices;
	vector<int> sales;

};

#endif

#include "menu.h"

Menu :: Menu(): arrayIndex(0)
{
}

Menu :: Menu(const int & index): array(index), arrayIndex(index) 
{
}

Menu :: Menu(const int & index, const string & actor, const string & artist, const string & musician, const string & raffle): array(index), arrayIndex(index)
{
	ifstream actorF, artistF, musicianF, raffleF;

	actorF.open(actor);
	if(actorF)
		actorF >> list;

	artistF.open(artist);
	if(artistF)
		artistF >> array;

	musicianF.open(musician);
	raffleF.open(raffle);
	if(musicianF && raffleF)
		loadMusician(musicianF, raffleF);
}

Menu :: Menu(const int & index, istream & actor, istream & artist, istream & musician, istream & raffle): array(index), arrayIndex(index)
{
	loadMusician(musician, raffle);
}


Menu :: ~Menu()
{
}

int Menu :: loadActor(istream & actorF)
{
	if(actorF)
	{
		actorF.clear();
		actorF.seekg(0);
		
		ActorNode * actor;
		list.removeAll();
		actor = new ActorNode(actorF);
		if(actorF.eof())
			return -1;
		
		while(!actorF.eof())
		{
			list.insertActor(actor);
			actor = new ActorNode(actorF);
		}

		delete actor;
		return 1;		
	}
	else
		return 0;
}

int Menu :: loadArtist(istream & artistF)
{
	if(artistF)
	{
		artistF.clear();
		artistF.seekg(0);
		
		ArtistNode * artist;
		array.removeAll();
		artist = new ArtistNode(artistF);
		if(artistF.eof())
			return -1;
		
		while(!artistF.eof())
		{
			array.insertArtist(artist);
			artist = new ArtistNode(artistF);
		}
		delete artist;
		return 1;		
	}
	else
		return 0;
}	

int Menu :: loadMusician(istream & musicianF, istream & raffleF)
{
	if(musicianF && raffleF)
	{
		musicianF.clear();
		musicianF.seekg(0);
		
		raffleF.clear();
		raffleF.seekg(0);
		
		Musician * musician;
		vec.clear();

		musician = new Musician(musicianF);
		if(musicianF.eof() || raffleF.eof())
			return -1;
		
		while(!musicianF.eof())
		{
			vec.push_back(*musician);
			delete musician;
			musician = new Musician(musicianF);
		}
		delete musician;
		return 1;		
	}
	else
		return 0;
}	

int Menu :: run()
{
	char option;
	topMenuSelect(option);
	return 0;
}

int Menu :: topMenuSelect(char & option)
{
	do
	{
		printTopMenu();
		option = getChar();
		topMenuRead(option);
	}
	while(option != 'q');
	return 1;
}

int Menu :: topMenuRead(char & option)
{
	switch(option)
	{
		case 'a':
			actorMenuSelect(option);
			break;
		case 'b':
			artistMenuSelect(option);
			break;
		case 'c':
			musicianMenuSelect(option);
			break;
		case 'q':
			break;
		default:
			cout << "Invalid option..." << endl;
			break;
	}	
	return 1;
}

int Menu :: actorMenuSelect(char & option)
{
	do
	{
		printActorMenu();
		option = getChar();
		actorMenuRead(option);
	}
	while(option != 'q' && option != '<');

	if(option == '<')
		option = 'a';
	return 1;
}

int Menu :: actorMenuRead(char & option)
{
	switch(option)
	{
		case'a':
			displayActor();
			break;
		case 'b':
			insertActor();
			break;
		case 'c':
			retrieveActor(option);
			break;
		case 'd':
			removeActor();
			break;
		case 'e':
			destroyActor(option);
			break;
		case 'f':
			loadActor(option);
			break;
		case 'g':
			saveActor();
			break;
		case 'q':
			break;
		case '<':
			break;
		default:
			cout << "Invalid option..." << endl;
	}
	return 1;
}

int Menu :: displayActor()
{
	int result = list.display();
	if(result == 0)
		cout << "List is empty. Please initialize List." << endl;
	return result;
}

int Menu :: insertActor()
{
	ActorNode * actor= nullptr; 
	createActor(actor);
	return list.insertActor(actor);
}

int Menu :: retrieveActor(char & option)
{
	ActorNode * actor = nullptr;
	cout << "Please enter index selection: ";
	int selection = getInt();
	int result = list.retrieveActor(selection, actor);
	if(result == 1)
		manipActor(actor, option);
	if(result == 0)
		cout << "No actor found." << endl;
	if(result == -1)
	cout << "No list. Please initialze list." << endl;
	if(result == -2)
		cout << "Index must be an integer greater than 0." << endl;
	return result;
}

int Menu :: removeActor()
{
	cout << "Please enter index selection: ";
	int selection = getInt();
	int result = list.removeActor(selection);
	if(result == 1)
		cout << "Actor deleted..." << endl;
	if(result == 0)
		cout << "No actor found." << endl;
	if(result == -1)
		cout << "No list. Please initialze list." << endl;
	if(result == -2)
		cout << "Index must be an integer greater than 0." << endl;
	return result;
}

int Menu :: destroyActor(char & option)
{
	int result = 0;
	cout << "This action will delete all current data. Are you sure? (y/n): ";
	bool choice = yesNo(option);
	if(choice)
		result = list.removeAll();
	if(result == 0)
		cout << "List is currently empty..." << endl;
	return result;
}

int Menu :: loadActor(char & option)
{
	ifstream input;
	cout << "This action will delete all current data. Are you sure? (y/n): ";
	bool choice = yesNo(option);
	if(choice)
	{
		input.open("actorLoadFile.txt");
		if(input)
		{
			list.removeAll();
			input >> list;
			input.close();
			return 1;
		}
		else 
		{
			input.close();
			return 0;
		}
	}
	else
		return 0;
}

int Menu :: saveActor()
{
	ofstream output;
	output.open("actorSaveFile.txt");
	if(output)
	{
		output << list;
		output.close();
		return 1;
	}
	else
	{
		output.close();
		return 0;
	}
}

int Menu :: artistMenuSelect(char & option)
{
	do
	{
		printArtistMenu();
		option = getChar();
		artistMenuRead(option);
	}
	while(option != 'q' && option != '<');

	if(option == '<')
		option = 'a';
	return 1;
}	

int Menu :: artistMenuRead(char & option)
{
	switch(option)
	{
		case'a':
			displayArtist();
			break;
		case 'b':
			insertArtist();
			break;
		case 'c':
			retrieveArtist(option);
			break;
		case 'd':
			removeArtist();
			break;
		case 'e':
			destroyArtist(option);
			break;
		case 'f':
			loadArtist(option);
			break;
		case 'g':
			saveArtist();
			break;
/*		case 'h':
			createArray();
			break;
*/		case 'q':
			break;
		case '<':
			break;
		default:
			cout << "Invalid option..." << endl;
	}
	return 1;
}

int Menu :: displayArtist()
{
	int result = array.display();
	if(result == 0)
		cout << "List is empty. Please initialize List." << endl;
	return result;
}

int Menu :: insertArtist()
{
	ArtistNode * artist= nullptr; 
	createArtist(artist);
	return array.insertArtist(artist);
}

int Menu :: retrieveArtist(char & option)
{
	ArtistNode * artist = nullptr;
	cout << "Please enter index selection: ";
	int selection = getInt();
	int booth = getInt();
	int result = array.retrieveArtist(selection, booth,  artist);
	if(result == 1)
		manipArtist(artist, option);
	if(result == 0)
		cout << "No artist found." << endl;
	if(result == -1)
	cout << "No list. Please initialze list." << endl;
	if(result == -2)
		cout << "Index must be an integer greater than 0." << endl;
	return result;
}

int Menu :: removeArtist()
{
	cout << "Please enter index selection: ";
	int selection = getInt();
	int booth = getInt();
	int result = array.removeArtist(selection, booth);
	if(result == 1)
		cout << "Artist deleted..." << endl;
	if(result == 0)
		cout << "No artist found." << endl;
	if(result == -1)
		cout << "No list. Please initialze list." << endl;
	if(result == -2)
		cout << "Index must be an integer greater than 0." << endl;
	return result;
}

int Menu :: destroyArtist(char & option)
{
	int result =0;
	cout << "This action will delete all current data. Are you sure? (y/n): ";
	bool choice = yesNo(option);
	if(choice)
		result = array.removeAll();
	if(result == 0)
		cout << "List is currently empty..." << endl;
	return result;
}

int Menu :: loadArtist(char & option)
{
	ifstream input;
	cout << "This action will delete all current data. Are you sure? (y/n): ";
	bool choice = yesNo(option);
	if(choice)
	{
		input.open("artistLoadFile.txt");
		if(input)
		{
			array.removeAll();
			input >> array;
			input.close();
			return 1;
		}
		else
		{
			input.close();
			return 0;
		}
	}
	else
		return 0;
}

int Menu :: saveArtist()
{
	ofstream output;
	output.open("artistSaveFile.txt");
	if(output)
	{
		output << array;
		output.close();
		return 1;
	}
	else
	{
		output.close();
		return 0;
	}
}

int Menu :: musicianMenuSelect(char & option)
{
	do
	{
		printMusicianMenu();
		option = getChar();
		musicianMenuRead(option);
	}
	while(option != 'q' && option != '<');

	if(option == '<')
		option = 'a';
	return 1;
}

int Menu :: musicianMenuRead(char & option)
{
	switch(option)
	{
		case'a':
			displayMusician();
			break;
		case 'b':
			insertMusician();
			break;
		case 'c':
			retrieveMusician(option);
			break;
		case 'd':
			removeMusician();
			break;
		case 'e':
			destroyMusician(option);

			break;
		case 'f':
			loadMusician(option);
			break;
		case 'g':
			saveMusician();
			break;
		case 'q':
			break;
		case '<':
			break;
		default:
			cout << "Invalid option..." << endl;
	}
	return 1;
}

int Menu :: displayMusician()
{
	if(vec.empty())
		cout << "List is empty. Please initialize List." << endl;
	else
	{
		int index = 0;
		for(auto it = vec.cbegin(); it != vec.cend(); it++)
		{
			index++;
			cout << index << ".\n";
			it->display();
			it->displayPrices();
			cout << endl;
		}
	}
	return 1;
}

int Menu :: insertMusician()
{
	Musician* musician = nullptr;
	createMusician(musician);
	vec.push_back(*musician);
	sort(vec.begin(), vec.end());
	return 1;
}

int Menu :: retrieveMusician(char & option)
{
	Musician * musician = nullptr;
	cout << "Please enter index selection: ";
	int selection = getInt()- 1;
	if(vec.empty())
	{
		cout << "Vector is empty..." << endl;
		return 0;
	}
	else
	{
		if(selection >= 0 && selection < (int) vec.size())
		{
			musician = &(vec.at(selection));
			manipMusician(musician, option);
			return 1;
		}
		else
		{
			cout << "Invalid selection..." << endl;
			return -1;
		}
	}
}

int Menu :: removeMusician()
{
	cout << "Please enter index selection: ";
	int selection = getInt() - 1;
	if(vec.empty())
		cout << "Vector is empty..." << endl;
	if(selection >= 0 && selection < (int) vec.size())
	{
		vec.erase(vec.cbegin()+selection);
		cout << "Musician deleted..." << endl;
		return 1;
	}
	else
	{
		cout << "Invalid selection..." << endl;
		return 1;
	}
}

int Menu :: destroyMusician(char & option)
{
	cout << "This action will delete all current data. Are you sure? (y/n): ";
	bool choice = yesNo(option);
	if(choice)
	{
		if(vec.empty())
		{
			cout << "List is currently empty..." << endl;
			return 0;
		}
		else
		{
			vec.clear();
			return 1;
		}
	}
	else
		return 0;
}

int Menu :: loadMusician(char & option)
{
	ifstream input;
	cout << "This action will delete all current data. Are you sure? (y/n): ";
	bool choice = yesNo(option);
	if(choice)
	{
		input.open("musicianLoadFile.txt");
		if(input)
		{
			vec.clear();
			Musician * musician = new Musician(input);

			while(!input.eof())
			{
				vec.push_back(*musician);
				delete musician;
				musician = new Musician(input);
			}
			delete musician;
			input.close();
			return 1;
		}
		else 
		{
			input.close();
			return 0;
		}
	}
	else
		return 0;
}

int Menu :: saveMusician()
{
	ofstream output;
	output.open("musicianSaveFile.txt");
	if(output)
	{
		output << list;
		output.close();
		return 1;
	}
	else
	{
		output.close();
		return 0;
	}
}


int Menu :: createActor(ActorNode *& actor) const
{
	cout << "Enter credentials..." << endl;
	cout << "Name: ";
	string name = getText();
	cout << "Section: ";
	int section = getInt();
	cout << "Booth: ";
	int booth = getInt();
	cout << "Start Time: ";
	int start = getInt();
	cout << "Promotion; ";
	string promo = getText();
	cout << "# Slots Available: ";
	int slots = getInt();
	if(actor)
		delete actor;
	Actor person(name, section, booth, start, promo, slots);	
	actor = new ActorNode(person);
	return 1;
}

int Menu :: manipActor(ActorNode*& actor, char & option)
{
	if(actor)
	{
		do
		{
			cout << endl;
			actor->display();
			printActorManip();
			option = getChar();
			actorActions(actor, option);
		}
		while(option != 'q' && option != '<');
		if(option == '<')
			option = 'a';
		return 1;
	}
	else
		return -1;	
}

int Menu :: actorActions(ActorNode *& actor, char & option)
{
	switch(option)
	{
		case 'a':
			actor->displaySched();
			break;
		case 'b':
			actor->slotsFull();
			break;
		case 'c':
			fillSlot(actor);
			break;
		case 'd':
			clearSlot(actor);
			break;
		case 'e':
			addSlot(actor);
			break;
		case 'f':
			break;
		case '<':
			break;
		case 'q':
			break;
		default:
			cout << "Invalid option..." << endl;
	}
	return 1;	

}

int Menu :: slotsFull(ActorNode *& actor)
{
	int result = actor->slotsFull();
	if(result == 1)
		cout << "There are currently no available slots." << endl;
	if(result ==0)
		cout << "There are currently slots avaiable!" << endl;
	return result;
}

int Menu :: fillSlot(ActorNode *& actor)
{
	cout << "Select Index: ";
	int selection = getInt();
	int result = actor->fillSlot(selection);

	if(result == 0)
		cout << "Slot " << selection << " currently unavailable." << endl;
	if(result == -1)
		cout << "Invalid selection..." << endl;
	if(result == 1)
		cout << "Slot has been updated successfully!" << endl;
	return result;
}

int Menu :: clearSlot(ActorNode *& actor)
{
	cout << "Select Index: ";
	int selection = getInt();
	int result = actor->clearSlot(selection);
	if(result == 0)
		cout << "Slot already empty." << endl;
	if(result == 1)
		cout << "Slot has been cleared successfully!" << endl;
	if(result == -1)
		cout << "Invalid selection..." << endl;
	return result;
}

int Menu :: addSlot(ActorNode *& actor)
{
	cout << "How many slots: " << endl;
	int selection = getInt();
	int result = actor->addSlots(selection);
	if(result == 1)
		cout << selection << " slots have been added!" << endl;
	else
		cout << "Number must be a positive integer." << endl;
	return result;
}

int Menu :: updateStart(ActorNode *& actor)
{
	cout << "Enter new start time: ";
	int start = getInt();
	int result = actor->updateStart(start);
	if(result == 1)	
		cout << "Slots have been updated.";
	return result;
}
	
int Menu :: createArtist(ArtistNode*& artist) const
{

	cout << "Enter credentials..." << endl;
	cout << "Name: ";
	string name = getText();
	cout << "Section: ";
	int section = getInt();
	cout << "Booth: ";
	int booth = getInt();
	cout << "# Raffle Prizes: ";
	int prizes = getInt();
	string option;
	vector<string> options;
	for(int i = 0; i<prizes; i++)
	{
		cout << "Prize " << i + 1 << " Descrip: ";
		options.push_back(option);
	}
	if(artist)
		delete artist;
	Artist person(name, section, booth, prizes, options);	
	artist =  new ArtistNode(person);
	return 1;
}

int Menu :: createRaffle(Raffle*& raffle) const
{

	cout << "Enter credentials..." << endl;
	cout << "Name: ";
	string name = getText();
	cout << "Email: ";
	string email = getText();
	cout << "Phone: ";
	string phone = getText();
	if(raffle)
		delete raffle;
	raffle =  new Raffle(name, email, phone);
	return 1;
}

int Menu :: manipArtist(ArtistNode*& artist, char & option)
{
	if(artist)
	{
		do
		{
			cout << endl;
			artist->display();
			printArtistManip();
			option = getChar();
			artistActions(artist, option);
		}
		while(option != 'q' && option != '<');
		if(option == '<')
			option = 'a';
		return 1;
	}
	else
		return -1;	
}

int Menu :: artistActions(ArtistNode *& artist, char & option)
{
	switch(option)
	{
		case 'a':
			purchaseRaffle(artist);
			break;
		case 'b':
			probability(artist);
			break;
		case 'c':
			pickWinner(artist);
			break;
		case '<':
			break;
		case 'q':
			break;
		default:
			cout << "Invalid option... "<< endl;
	}
	return 1;
}

int Menu :: purchaseRaffle(ArtistNode *& artist)
{
	cout << "Select prize: ";
	int selection = getInt();
	Raffle * entry = nullptr;
	createRaffle(entry);
	if(artist->purchaseRaffle(selection, *entry)  <= 0)
		cout << "Error... Invalid prize selection." << endl;
	return 1;
}

int Menu :: probability(ArtistNode *& artist)
{
	cout << "Select prize: ";
	int selection = getInt();
	return artist->probability(selection);
}

int Menu :: pickWinner(ArtistNode *& artist)
{
	cout << "Select prize: ";
	int selection = getInt();
	return artist->generateWin(selection);	
}

int Menu :: createMusician(Musician*& musician) const
{
	cout << "Enter credentials..." << endl;
	cout << "Name: ";
	string name = getText();
	cout << "Section: ";
	int section = getInt();
	cout << "Booth: ";
	int booth = getInt();
	cout << "# Ticket Tiers: ";
	int tier = getInt();
	string option;
	double price;
	int capac;
	vector<string> options;
	vector<double> prices;
	vector<int> cap;
	for(int i = 0; i<tier; i++)
	{
		cout << "Option " << i + 1 << endl <<"Descrip: ";
		option = getText();
		cout << "Price: ";
		price = getDouble();
		cout << "Capacity: ";
		capac = getInt();
		options.push_back(option);
		prices.push_back(price);
		cap.push_back(capac);
	}
	if(musician)
		delete musician;
	Musician person(name, section, booth, tier, options, cap, prices);
	musician = new Musician(person);
	return 1;
}

int Menu :: manipMusician(Musician*& musician, char & option)
{
	if(musician)
	{
		do
		{
			cout << endl;
			musician->display();
			musician->displayPrices();
			printMusicianManip();
			option = getChar();
			musicianActions(musician, option);
		}
		while(option != 'q' && option != '<');
		if(option == '<')
			option = 'a';
		return 1;
	}
	else
		return -1;	
}

int Menu :: musicianActions(Musician *& musician, char & option)
{
	switch(option)
	{
		case 'a':
			soldOut(musician);
			break;
		case 'b':
			purchaseTicket(musician);
			break;
		case 'c':
			calcRev(musician);
			break;
		case 'd':
			capIncrease(musician);
			break;
		case 'e':
			updateOptions(musician);
			break;
		case '<':
			break;
		case 'q':
			break;
		default:
			cout << "Invalid option... "<< endl;
	}
	return 1;
}

int Menu :: soldOut(Musician *& musician)
{
	cout << "Choose a ticket Tier: ";
	int selection = getInt();
	int result = musician->soldOut(selection);
	if(result == 0)
		cout << "There are still tickets for purchase."<< endl;
	if(result == -1)
		cout << "Error, update pricing." << endl;
	if(result == -2)
		cout << "Error, invalid selection." << endl;
	if(result == 1)
		cout << "The show is sold out!" << endl;
	return result;
}

int Menu :: purchaseTicket(Musician *& musician)
{
	cout << "Choose a ticket Tier: ";
	int selection = getInt();
	cout << "Choose purchase amount: ";
	int amount = getInt();

	int result = musician->purchaseTicket(selection, amount);
	if(result == 0)
		cout << "This tier is sold out."<< endl;
	if(result == -1)
		cout << "Error, update pricing." << endl;
	if(result == -2)
		cout << "Error, invalid selection." << endl;
	if(result == 1)
		cout << amount << " tickets have been purchased!" << endl;
	return result;	
}
		
int Menu :: calcRev(Musician *& musician)
{
	int result = musician->calculateRev();
	if(result == -2 || result == -1)
		cout << "Error, please update ticket tiers." << endl;;
	return result;
}

int Menu :: capIncrease(Musician *& musician)
{
	cout << "Choose a ticket Tier: ";
	int selection = getInt();
	cout << "Choose capacity increase: ";
	int increase = getInt();
	int result = musician->increaseCap(increase, selection);
	if(result == 1)
		cout << "Capacity increased successfully!" << endl;
	if(result == 0)
		cout << "Invalid selection..." << endl;
	if(result == -1)
		cout << "Error, please update ticket tiers." << endl;
	return result;
}

int Menu :: updateOptions(Musician *& musician)
{
	cout << "# Ticket Tiers: ";
	int tier = getInt();
	string option;
	double price;
	int capac;
	vector<string> options;
	vector<double> prices;
	vector<int> cap;
	for(int i = 0; i<tier; i++)
	{
		cout << "Option " << i + 1 << endl <<"Descrip: ";
		option = getText();
		cout << "Price: ";
		price = getDouble();
		cout << "Capacity: ";
		capac = getInt();
		options.push_back(option);
		prices.push_back(price);
		cap.push_back(capac);
	}
	int result = musician->updateOptions(tier, options, prices, cap);
	if(result == 1)
		cout << "Tiers options have been updated." << endl;	
	else
		cout << "Error updating tiers. Try again." << endl;
	return result;
}
 
int Menu :: printTopMenu() const
{
	cout << endl;
	cout << "A) Actor Menu" << endl;
	cout << "B) Artist Menu" << endl;
	cout << "C) Musician Menu" << endl;
	cout << "Q) Quit" << endl;
	cout << "Selection: ";
	
	return 1;
}

int Menu :: printActorMenu() const
{
	cout << endl;
	cout << "A) Display Actor" << endl;
	cout << "B) Insert Actor" << endl;
	cout << "C) Retrieve Actor" << endl;
	cout << "D) Remove Actor" << endl;
	cout << "E) Remove All" << endl;
	cout << "F) Load Actor" << endl;
	cout << "G) Save Actor" << endl;
	cout << "Q) Quit" << endl;
	cout << "<) Previous Menu" << endl;
	cout << "Selection: ";
		
	return 1;
}

int Menu :: printArtistMenu() const
{
	cout << endl;
	cout << "A) Display Artist" << endl;
	cout << "B) Insert Artist" << endl;
	cout << "C) Retrieve Artist" << endl;
	cout << "D) Remove Artist" << endl;
	cout << "E) Remove All" << endl;
	cout << "F) Load Artist" << endl;
	cout << "G) Save Artist" << endl;
	cout << "Q) Quit" << endl;
	cout << "<) Previous Menu" << endl;
	cout << "Selection: ";
		
	return 1;
}

int Menu :: printMusicianMenu() const
{
	cout << endl;
	cout << "A) Display Musician" << endl;
	cout << "B) Insert Musician" << endl;
	cout << "C) Retrieve Musician" << endl;
	cout << "D) Remove Musician" << endl;
	cout << "E) Remove All" << endl;
	cout << "F) Load Musician" << endl;
	cout << "G) Save Musician" << endl;
	cout << "Q) Quit" << endl;
	cout << "<) Previous Menu" << endl;
	cout << "Selection: ";
		
	return 1;
}

int Menu :: printActorManip() const
{
	cout << endl;
	cout << "A) Display Schedule" << endl;
	cout << "B) Check For Open Slots" << endl;
	cout << "C) Fill a Slot" << endl;
	cout << "D) Clear a Slot" << endl;
	cout << "E) Add Slots" << endl;
	cout << "F) Update Start Time" << endl;
	cout << "<) Previous Menu" << endl;
	cout << "Q) Quit" << endl;
	cout << "Selection: ";

	return 1;
}

int Menu :: printArtistManip() const
{
	cout << endl;
	cout << "A) Purchase Raffle" << endl;
	cout << "B) Calculate Probability" << endl;
	cout << "C) Generate Winner" << endl;
	cout << "<) Previous Menu" << endl;
	cout << "Q) Quit" << endl;
	cout << "Selection: ";

	return 1;
}

int Menu :: printMusicianManip() const
{
	cout << endl;
	cout << "A) Sold Out?" << endl;
	cout << "B) Purchase Ticket" << endl;
	cout << "C) Calculate Revenue" << endl;
	cout << "D) Increase Capacity" << endl;
	cout << "E) Update Options" << endl;
	cout << "<) Previous Menu" << endl;
	cout << "Q) Quit" << endl;
	cout << "Selection: ";

	return 1;
}



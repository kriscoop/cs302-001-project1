/*
Name: Kristian Cooper
Class#: CS-302-001
Project1
ALL.h This class is a derived class from the person class. It will have additional methods that allow user interaction such as checking if there are available slots, filling a slot, and clearing a slot. 
*/


#ifndef ALL_H
#define ALL_H


#include "artistNode.h"

class ALL
{
	friend istream & operator>> (istream & input, ALL & list);
	friend ostream & operator<< (ostream & input, ALL & list);
public:
	ALL();
	ALL(const int & MAX); 
	ALL(ArtistNode * & aNode, const int & MAX); 
	ALL(const ArtistNode & artist, const int & MAX);
	~ALL();

	int retrieveArtist(const int & section, const int & num, ArtistNode *& aNode);
	int insertArtist(ArtistNode * & aNode);
	int display() const;
	int removeArtist(const int & section, const int & num);
	int removeAll();
	
	ALL (const ALL & list);
	ALL & operator=(const ALL & list);
	
private:
	int size; 
	ArtistNode ** head;

	//Recursive Functions
	ArtistNode * & retrieveArtist(ArtistNode * & aNode, const int & num, int & position);
	ArtistNode * & retrieveArtist(ArtistNode * & head, const int & section, const int & num, int & position);
	int insertArtist(ArtistNode * & head, ArtistNode * & aNode);
	int insertArtist(ArtistNode * & head, int & index, const int & section, ArtistNode *& aNode);
	int destroy();
	void destroy(ArtistNode * & head, int & index);
	void destroy(ArtistNode * & head);
	void display(ArtistNode * & head, int & index) const;
	void display(ArtistNode * & head, int & index, int & position) const;
	int removeArtist(ArtistNode * & head, const int & section, const int & num, int & index);
	int removeArtist(ArtistNode * & current, ArtistNode *& prev, const int & num, int & position);
	int copy(ArtistNode ** current, int & index);
	ArtistNode * & copy(ArtistNode * & current);
	int printToFile(ArtistNode **& head, const int & size, int & index, ostream & stream);
	int printToFile(ArtistNode *& current, ostream & stream);
};

#endif

/*
name: Kristian Cooper
Class#: CS-302-001 
Project1
raffle.h
This is a derived class from person to store customer raffle data. This classes job is to store customer data for raffle ticket purchase. There are no keys due to the method of winner generation. Winner will be chosen by modding a rand number by the LLL length of raffle objects.  
*/

#ifndef ARTISTNODE_H
#define ARTISTNODE_H

#include "artist.h"


class ArtistNode: public Artist
{
	friend istream & operator>>(istream & stream, ArtistNode & artist);
	friend ostream & operator<<(ostream & stream, ArtistNode & artist);

public:
	ArtistNode();
	ArtistNode(istream & istream);
	ArtistNode(ArtistNode *& artist);
	ArtistNode(const Artist & artist);
	~ArtistNode();

	ArtistNode*& getNext();
	int updateArtist(const Artist & artist);
	int setNext(ArtistNode * & aNode);

	int display() const;
	ArtistNode(const ArtistNode & aNode);
	ArtistNode& operator=(const ArtistNode & aNode);
protected:

	ArtistNode * next;
};

#endif

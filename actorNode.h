/*
name: Kristian Cooper
Class#: CS-302-001 
Project1
raffle.h
This is a derived class from person to store customer raffle data. This classes job is to store customer data for raffle ticket purchase. There are no keys due to the method of winner generation. Winner will be chosen by modding a rand number by the LLL length of raffle objects.  
*/

#ifndef ACTORNODE_H
#define ACTORNODE_H

#include "actor.h"


class ActorNode: public Actor
{
	friend istream & operator>>(istream & input, ActorNode & actor);
	friend ostream & operator<<(ostream & input, ActorNode & actor);

public:
	ActorNode();
	ActorNode(istream & input);
	ActorNode(ActorNode * & actor);
	ActorNode(const Actor & actor);
	~ActorNode();

	ActorNode * & getNext();
	int updateActor(const Actor & actor);
	int setNext(ActorNode * & aNode);

	int display() const;
	ActorNode (const ActorNode & actor);
	ActorNode & operator=(const ActorNode & actor);

protected:

	ActorNode * next;
};

#endif

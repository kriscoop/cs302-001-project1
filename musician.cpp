#include "musician.h"


Musician :: Musician(): options(0)
{

}

Musician :: Musician(istream & input): Person(input), options(0)
{
	input >> *this;
}

Musician :: Musician(const string & aName, const int & section, const int & booth, const int & numOptions, const vector<string> & descrip, const vector<int> & cap, const vector<double> price): Person(aName, section, booth), options(numOptions), optionDescrip(descrip), capacity(cap), prices(price), sales(numOptions)
{
}


Musician :: ~Musician()
{
}

int Musician :: soldOut(const int & type) const
{
	if(sales.empty() || capacity.empty() || options <=0)
		return -1;
	else if(type-1 <= 0 || type-1 > options)
		return -2;
	else if(sales.at(type-1) >= capacity.at(type-1))
		return 1;
	else
		return 0;
}

int Musician :: purchaseTicket(const int & quantity, const int & type)
{
	if(sales.empty() || capacity.empty() || options <=0)
		return -1;
	else if(type-1 < 0 || type-1 > options)
	{
		return -2;
	}
	else if(sales.at(type-1) >= capacity.at(type-1))
		return 0;
	sales.at(type-1) += quantity;
	return 1;
}

int Musician :: calculateRev() const
{
	double rev = 0;
	if(sales.empty() || capacity.empty() || options <=0)
		return -1;
	for(int i = 0; i < options; i++)
	{
		if(!sales.at(i) || !prices.at(i))
			return -2;
		rev += sales.at(i) * prices.at(i);
	}

	cout << "The total revenue from all sales is $" << rev << "." << endl;
	return 1;
}

int Musician :: increaseCap(const int & cap, const int & type)
{
	if(sales.empty() || capacity.empty() || options <=0)
		return -1;
	else if(type-1 < 0 || type-1 > options)
		return 0;
	else
	{
		capacity.at(type) += cap;
		return 1;
	}
}

int Musician :: updateOptions(const int & options, vector<string> tier, vector<double> price, vector<int> cap)
{
	if(options < this->options || tier.empty() || price.empty() || cap.empty())
		return 0;
	this->options = options;
	for(int i = 0; i < options;i++)
	{
		optionDescrip.insert(optionDescrip.cbegin()+i, tier.at(i));
		prices.insert(prices.cbegin()+i, price.at(i));
		capacity.insert(capacity.cbegin()+i, cap.at(i));
	}
	return 1;
}

int Musician :: display() const
{
	Person::display();
	return 1;
}

int Musician :: displayPrices() const
{
	if(optionDescrip.empty() || prices.empty() || options <=0)
		return -1;
	else
	{
		cout << "PRICING" << endl;
		for(int i = 0; i<options; i++)
			cout << i+1 << "\t" << optionDescrip.at(i) << ".\t" << prices.at(i) << endl;
		return 1;
	}
}

istream& operator>> (istream& stream, Musician & musician)
{
	char buffer[MAX_STRING];
	stream >> musician.options;
	stream.ignore(1, ';');

	musician.optionDescrip.clear();

	for(int i = 0; i<musician.options; i++)
	{
		if(i < musician.options - 1)
			stream.getline(buffer, MAX_STRING, ';');
		else
			stream.getline(buffer, MAX_STRING, '\n');
		musician.optionDescrip.insert(musician.optionDescrip.cbegin()+i, buffer);
	}

	musician.capacity.clear();
	musician.prices.clear();
	musician.sales.clear();

	int num = 0;
	double price = 0;

	for(int i = 0; i<musician.options; i++)
	{

		stream >> num;
		musician.capacity.insert(musician.capacity.cbegin()+i, num);
		stream.ignore(1, ';');

		stream >> price;
		musician.prices.insert(musician.prices.cbegin()+i, price);
		stream.ignore(1, ';');
		
		stream >> num; 
		musician.sales.insert(musician.sales.cbegin()+i, num);
		
		if(i < musician.options -1)
			stream.ignore(1, ';');
		else
			stream.ignore(1, '\n');
	}

	return stream;
}

ostream& operator<< (ostream& stream, Musician & musician)
{
	stream << musician.options << ';';
	for(int i = 0; i<musician.options; i++)
	{
		stream << musician.optionDescrip.at(i);
		if(i < musician.options - 1)
			stream << ';';
		else
			stream << '\n';
	}
	
	for(int i = 0; i<musician.options; i++)
	{
		stream << musician.capacity.at(i) << ';';
		stream << musician.prices.at(i) << ';';
		stream << musician.sales.at(i);
		if(i < musician.options - 1)
			stream << ';';
		else
			stream << '\n';
	}
	
	return stream;
}
		
	


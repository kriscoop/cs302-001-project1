#include "actorNode.h"

ActorNode :: ActorNode(): next(nullptr)
{
}

ActorNode :: ActorNode(istream & input): Actor(input), next(nullptr)
{
	input >> *this;
}

ActorNode :: ActorNode(ActorNode * & actor): Actor(*actor), next(nullptr)
{
}

ActorNode :: ActorNode(const Actor & actor): Actor(actor), next(nullptr)
{
}

ActorNode :: ~ActorNode()
{
}

ActorNode*& ActorNode :: getNext()
{
	return next;
}

int ActorNode :: updateActor(const Actor & actor)
{
	Actor::operator=(actor);
	return 1;
}

int ActorNode :: setNext(ActorNode * & aNode)
{
	next = aNode;
	return 1;
}

int ActorNode :: display() const
{
	Actor::display();
	return 1;
}

ActorNode :: ActorNode (const ActorNode & to_copy): Actor(to_copy), next(nullptr)
{
}

ActorNode & ActorNode :: operator=(const ActorNode & to_copy)
{
	Actor::operator=(to_copy);
	next = nullptr;

	return *this;
}

istream & operator >>(istream & stream, ActorNode & actor)
{
//	stream >> (Actor&) actor;

	return stream;
}

ostream & operator << (ostream & stream, ActorNode& actor)
{
//	stream << (Actor&) actor;

	return stream;
}


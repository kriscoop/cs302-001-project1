#include "musicianNode.h"

MusicianNode :: MusicianNode(): next(nullptr)
{
}

MusicianNode :: ~MusicianNode()
{
	if(next)
		delete next;
}

MusicianNode* MusicianNode :: getNext()
{
	return next;
}

const int MusicianNode :: updateMusician(const Musician & musician)
{
	Musician::operator=(musician);
	return 1;
}

const int MusicianNode :: setNext(MusicianNode * & aNode)
{
	if(next)
		delete next;
	next = aNode;
	return 1;
}

const int MusicianNode :: display() const
{
	Musician::display();
	return 1;
}

MusicianNode :: MusicianNode(const MusicianNode & to_copy): Musician(to_copy)
{
	next = nullptr;
}

MusicianNode & MusicianNode :: operator=(const MusicianNode & to_copy)
{
	if(this == &to_copy)
		return *this;
	Musician::operator=(to_copy);

	if(next);
		delete next;
	next = nullptr;
	return *this;
}


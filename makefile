CC = g++
CPPFLAGS = -Wall -g -std=c++17

project1:	cooper-kristian-Project1.o person.o raffle.o actor.o artist.o musician.o actorNode.o artistNode.o musicianNode.o
	g++ cooper-kristian-Project1.o person.o raffle.o actor.o artist.o musician.o actorNode.o artistNode.o musicianNode.o -o project1

cooper-kristian-Project1.o:	cooper-kristian-Project1.cpp
	g++ -c cooper-kristian-Project1.cpp

musicianNode.o:	musicianNode.cpp
	g++ -c musicianNode.cpp

artistNode.o:	artistNode.cpp
	g++ -c artistNode.cpp

actorNode.o:   actorNode.cpp
	g++ -c actorNode.cpp

musician.o:	musician.cpp
	g++ -c musician.cpp
artist.o:	artist.cpp
	g++ -c artist.cpp

actor.o:	actor.cpp
	g++ -c actor.cpp

raffle.o:	raffle.cpp
	g++ -c raffle.cpp

person.o:	person.cpp
	g++ -c person.cpp

clean:
	$(info -- cleaning the direcotry --)
	rm project1
	rm -f *.o

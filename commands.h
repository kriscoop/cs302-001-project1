/*
name: Kristian Cooper
Class#: 260
Project2
commands.h
This is the header file contains various functions used in the driver program.
*/


#ifndef COMMANDS_H
#define COMMANDS_H


#include <string>
#include <cctype>
#include <iostream>


using namespace std;


const int MAX_STRING = 256;


bool validInput();
int getInt();
double getDouble();
string getText();
void getText(char buffer[]);
char getChar();
bool yesNo(char& option);



#endif

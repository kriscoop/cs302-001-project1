/*
name: Kristian Cooper
Class#: CS-302-001 
Project1
raffle.h
This is a derived class from person to store customer raffle data. This classes job is to store customer data for raffle ticket purchase. There are no keys due to the method of winner generation. Winner will be chosen by modding a rand number by the LLL length of raffle objects.  
*/

#ifndef RAFFLE_H
#define RAFFLE_H

#include "person.h"


class Raffle: public Person
{
	friend istream & operator >> (istream & stream, Raffle & raffle);
	friend ostream & operator << (ostream & stream, Raffle & raffle);
	
public:
	Raffle();
	Raffle(const string & aName, const string & anEmail, const string & aNum);
	~Raffle();

	int updateEmail(const string & aMail);
	int updateNumber(const string & aNum);

	int display() const;

protected:

	string email;
	string phone;
};

#endif

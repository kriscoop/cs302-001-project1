#include "artist.h"

Artist :: Artist(): prizes(0), prizeDescrip(0), entries(0)
{
	prizeDescrip.clear();
	entries.clear();
}

Artist :: Artist(istream & input): Person(input), prizes(0), prizeDescrip(0), entries(0)
{
	prizeDescrip.clear();
	entries.clear();
	input >> *this;
	entries.resize(prizes);
}

Artist :: Artist(const string & aName, const int & section, const int & booth, const int & prizeNum, const vector<string> descrips): Person(aName, section, booth), prizes(prizeNum), prizeDescrip(prizeNum), entries(prizeNum)
{
	for(int i = 0; i<prizeNum; i++)
		prizeDescrip.insert(prizeDescrip.cbegin()+i, descrips.at(i));
}

Artist :: ~Artist()
{

}

int Artist :: purchaseRaffle(const int & prize, const Raffle & aRaffle)
{
	if(prize-1 < (int) entries.size() && prize -1 >= 0)
	{
		entries.at(prize-1).push_back(aRaffle);
		return 1;
	}
	else
		return 0;
}

int Artist :: probability(const int & prize)
{
	if(prize-1 < (int) entries.size() && prize -1 >= 0)
	{
		vector<Raffle> choice = entries.at(prize-1);
		if(!choice.empty())
		{
			double entryNum = (double) choice.size();
			double prob = (1/entryNum) * 100;
			cout << "There is a 1/" << entryNum << " or " << fixed << setprecision(2) << prob << "% chance of winning prize " << prize << "." << endl;
			return 1;
		}
		else
		{
			cout << "There are currently no entries for this prize." << endl;
			return 0;
		}
	}
	else
	{
		cout << "Error, invalid prize selection." << endl;
		return -1;
	}
}

int Artist :: generateWin(const int & prize)
{
	if(prize-1 < (int) entries.size() && prize -1 >= 0)
	{
		vector<Raffle> choice = entries.at(prize-1);
		if(!choice.empty())
		{
			srand((unsigned) time(NULL));
			int winner = rand() % choice.size();

			cout << "The winner of prize " << prize << " is ..." << endl;
			choice.at(winner).display();
			cout << "Come and claim your prize!" << endl;

			entries.erase(entries.cbegin()+prize-1);
			prizeDescrip.erase(prizeDescrip.cbegin()+prize-1);
			prizes--;
			
			return 1;
		}
		else
		{
			cout << "There are currently no entries for this prize." << endl;
			return 0;
		}
	}
	else
	{
		cout << "Error, prize not found." << endl;
		return -1;
	}
}

int Artist :: display() const
{
	Person ::display();
	cout << "PRIZES" << endl;
	if(!prizeDescrip.empty())
	{
		for(int i = 0; i < prizes; i++)
			cout << i+1 << ".\t" << prizeDescrip.at(i) << endl;
		return 1;
	}
	else
		return -1;
}

istream& operator>> (istream& stream, Artist & artist)
{
	char buffer[MAX_STRING];
//	stream >> (Person&) artist;
	stream >> artist.prizes;
	stream.ignore(1, ';');

	artist.prizeDescrip.clear();
	
	for(int i = 0; i<artist.prizes; i++)
	{
		if(i < artist.prizes - 1)
			stream.getline(buffer, MAX_STRING, ';');
		else
			stream.getline(buffer, MAX_STRING, '\n');
		artist.prizeDescrip.push_back(buffer);
	}
	
	return stream;
}

ostream& operator<< (ostream& stream, Artist & artist)
{
	stream << (Person &) artist;
	stream << artist.prizes << ';';
	for(int i = 0; i<artist.prizes; i++)
	{
		if(i < artist.prizes - 1)
			stream << artist.prizeDescrip.at(i) << ';';
		else
			stream << artist.prizeDescrip.at(i) << '\n';
	}
	
	return stream;
}





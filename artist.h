/*
Name: Kristian Cooper
Class#: CS-302-001
Project1
artist.h
This class is a derived class from the person class. It will have additional methods that allow user interaction such as purchasing a raffle, calculating win probability, and generating winner. Once the winner for a price is chosen, the option will no longer be available and the entries will be cleared.   
*/

#ifndef ARTIST_H
#define ARTIST_H

#include "person.h"
#include "raffle.h"
#include <vector>
#include <cstdlib>

class Artist : public Person
{
	friend istream& operator>> (istream& stream, Artist & artist);
	friend ostream& operator<< (ostream& stream, Artist & artist);

public:
	Artist();
	Artist(istream & input);
	Artist(const string & aName, const int & section, const int & booth, const int & prizeNum, const vector<string> descrips);
	~Artist();

	int purchaseRaffle(const int & prize, const Raffle & aRaffle);
	int probability(const int & prize);
	int generateWin(const int & prize);
	
	int display() const;

protected:
	
	int prizes;
	vector<string> prizeDescrip;
	vector<vector<Raffle>> entries;
};

#endif

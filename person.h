/*
name: Kristian Cooper
Class#: CS-302-001 
Project1
person.h
This is the base class for the celebrity heirarchy among other things. (I've renamed this class to person instead of celebrity to encompass additional derived classes to house customer data as well
*/

#ifndef PERSON_H
#define PERSON_H

#include "commands.h"
#include <cstring>
#include <iomanip>
#include <fstream>

using namespace std;

class Person
{
	friend istream& operator>> (istream& stream, Person & aPerson);
	friend ostream& operator<< (ostream& stream, Person & aPerson);

public:
	Person();
	Person(const string & aName, const int & aSection, const int & aNum);
	Person(istream & input);
	~Person();

	int updateName(const string & aName);
	int findName(const string & aName);
	int updateLocation(const int & aSection, const int & aNum);
	int findLocation(const int & aSection, const int & aNum);
	int getSection() const;

	int display() const;
	Person(const Person & to_copy);
	Person & operator=(const Person & to_copy);
	bool operator<(const Person & to_copy);

protected:
	char * name;
	int section;
	int booth;

	int copy(char*& copy_to, const char* to_copy);
};


#endif


#include "actor.h"

Actor :: Actor() : startTime(0), promotion(nullptr), numSlots(0), slots(nullptr) 
{
}

Actor :: Actor(istream & input):Person(input), startTime(0), promotion(nullptr), numSlots(0), slots(nullptr)
{
	input >> *this;
}

Actor :: Actor(const string & aName, const int & section, const int & booth, const int & start, const string & aString, const int & slotNum): Person(aName, booth, section), startTime(start), promotion(nullptr), numSlots(slotNum), slots(nullptr)
{
	copy(promotion, aString.c_str());

	slots = new int[numSlots];
	for(int i = 0; i < numSlots; i++)
		slots[i] = 0;
}

Actor :: ~Actor()
{
	if(promotion)
		delete [] promotion;
	if(slots)
		delete [] slots;
}

int Actor :: slotsFull() const
{
	if(numSlots == 0)
		return -1;

	for(int i = 0;i < numSlots; i++)
		if(slots[i] != 0)
			return 0;
	return 1;	
}

int Actor :: addSlots(const int & aNum)
{
	if(aNum <= 0)
		return 0;

	int * update = new int[numSlots + aNum];

	if(slots)
	{
		for(int i = 0; i< numSlots; i++)
			update[i] = slots[i];
		for(int i = numSlots; i < numSlots+aNum; i++)
			update[i] = 0;
		numSlots += aNum;
		delete [] slots;
		slots = update;
	}
	else
	{
		slots = new int [numSlots + aNum];
		numSlots += aNum;
	}
	
	return 1;
}

int Actor :: updateStart(const int & aNum)
{
	if(aNum <= 0)
		return -1;
	else
	{
		startTime = aNum % 13;
		return 1;
	}
}

int Actor :: clearSlot(const int & aNum)
{
	if(slots && (aNum -1 < numSlots && aNum - 1 >= 0))
	{
		if(slots[aNum-1] != 0)
		{
			slots[aNum-1]=0;
			return 1;
		}
		else
			return 0;
	}
	return -1;
}

int Actor :: fillSlot(const int & aNum)
{
	if(slots && (aNum -1 < numSlots && aNum - 1 >= 0))
	{
		if(slots[aNum-1] == 0)
		{
			slots[aNum-1]=1;
			return 1;
		}
		else
       			return 0;
	}
	return -1;
}

int Actor :: display() const
{
	Person::display();
	if(promotion)
	{
		cout << "Featured: " << promotion << endl;
		return 1;
	}
	else
	{
		cout << "Featured: No current feature" << endl;
		return 0;
	}
}

int Actor :: displaySched() const
{
	if(slots)
	{
		int currentHour = startTime;
		int currentMin = 0;

		for(int i = 0; i < numSlots; i++)
		{
			cout << i + 1 << ".\t" << currentHour << ':' << setw(2) << setfill('0') << currentMin << '\t';
			if(slots[i] == 0)
				cout << "OPEN" << endl;
			else
				cout << "RESERVED" << endl;
			currentMin += 15;
			currentMin %= 60;
			if(currentMin ==0)
				currentHour++;
			currentHour %= 13;
		}
		return 1;
	}
	else
	{
		cout << "No schedule confirmed. Please add slots to schedule." << endl;
		return 0;
	}
}
	
Actor :: Actor(const Actor & to_copy): Person(to_copy)
{
	promotion = nullptr;
	copy(promotion, to_copy.promotion);
	numSlots = to_copy.numSlots;
	startTime = to_copy.startTime;

	slots = new int[numSlots];
	if(to_copy.slots)
	{
		for(int i = 0; i < numSlots; i++)
		{
			slots[i] = to_copy.slots[i];
		}
	}
	else
		slots = nullptr;
}

Actor & Actor :: operator=(const Actor & to_copy)
{
	if(this ==& to_copy)
		return *this;
	Person::operator=(to_copy);
	copy(promotion, to_copy.promotion);
        numSlots = to_copy.numSlots;

        if(slots)
                delete [] slots;

        slots = new int[numSlots];
        for(int i = 0; i < numSlots; i++)
        {
                slots[i] = to_copy.slots[i];
        }
	
	return *this;
}

istream& operator>> (istream& stream, Actor & actor)
{
	char buffer[MAX_STRING];
	stream >> actor.startTime;
	stream.ignore(1, ';');

	stream.getline(buffer, MAX_STRING, ';');
	actor.Person::copy(actor.promotion, buffer);
	
	stream >> actor.numSlots;
	stream.ignore(1, ';');

	if(actor.slots)
		delete actor.slots;

	actor.slots = new int[actor.numSlots];
	
	for(int i = 0; i<actor.numSlots; i++)
	{
		stream >> actor.slots[i];
		if(i <  actor.numSlots- 1)
			stream.ignore(1, ';');
		else
			stream.ignore(1, '\n');
	}

	return stream;
}

ostream& operator<< (ostream& stream, Actor & actor)
{
	stream << (Person &) actor;
	stream << actor.startTime << ';';
	stream << actor.promotion << ';';
	stream << actor.numSlots << ';';
	
	for(int i = 0; i<actor.numSlots; i++)
	{
		stream << actor.slots[i];
		if(i < actor.numSlots - 1)
			stream << ';';
		else
			stream << '\n';
	}
	
	return stream;
}




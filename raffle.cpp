#include "raffle.h"

Raffle :: Raffle(): Person(), email("ERROR"), phone(0)
{
}

Raffle :: Raffle(const string & aName, const string & anEmail, const string & aNum): Person(aName, '0', 0), email(anEmail), phone(aNum)
{
}

Raffle :: ~Raffle()
{
}

int Raffle :: updateEmail(const string & aMail)
{
	email = aMail;
	return 1;
}

int Raffle :: updateNumber(const string & aNum)
{
	phone = aNum;
	return 1;
}

int Raffle :: display() const
{
	cout << "Name: " << name << endl;
	cout << "Phone: " << phone << endl;
	cout << "Email: " << email << endl;
	return 1;
}

istream & operator >> (istream & stream, Raffle & raffle)
{
	char buffer[MAX_STRING];
	stream >> (Person&) raffle;

	stream >> raffle.phone;
	stream.ignore(1, ';');
	
	stream.getline(buffer, MAX_STRING, '\n');
	raffle.email = buffer;	
	
	return stream;
}
	
ostream & operator << (ostream & stream, Raffle & raffle)
{
	stream << (Person&) raffle;
	stream << raffle.phone << ';';
	stream << raffle.email << '\n';

	return stream;
}

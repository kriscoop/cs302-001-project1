#include "artistNode.h"

ArtistNode :: ArtistNode(): next(nullptr)
{
}

ArtistNode :: ArtistNode(istream & input): Artist(input), next(nullptr)
{
	input >> *this;
}

ArtistNode :: ArtistNode(ArtistNode * & artist): Artist(*artist), next(nullptr)
{
}

ArtistNode :: ArtistNode(const Artist & artist): Artist(artist), next(nullptr)
{
}

ArtistNode :: ~ArtistNode()
{
}

ArtistNode*& ArtistNode :: getNext()
{
	return next;
}

int ArtistNode :: updateArtist(const Artist & artist)
{
	Artist::operator=(artist);
	return 1;
}

int ArtistNode :: setNext(ArtistNode *& aNode)
{
	int result = 0;
	if(next)
		result = -1;
	else if(next == nullptr)
		result = 1;
	next = aNode;
	return result;
}

int ArtistNode :: display() const
{
	Artist::display();
	return 1;
}

ArtistNode :: ArtistNode(const ArtistNode & to_copy): Artist(to_copy)
{
	next = nullptr;
}

ArtistNode & ArtistNode :: operator=(const ArtistNode & to_copy)
{
	if(this == &to_copy)
		return *this;
	Artist::operator=(to_copy);

	next = nullptr;
	return *this;
}

istream & operator>>(istream & stream, ArtistNode& artist)
{
//	stream >> (Artist&) artist;

	return stream;
}

ostream & operator<<(ostream & stream, ArtistNode & artist)
{
//	stream << (Artist&) artist;

	return stream;
}

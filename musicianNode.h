/*
name: Kristian Cooper
Class#: CS-302-001 
Project1
raffle.h
This is a derived class from person to store customer raffle data. This classes job is to store customer data for raffle ticket purchase. There are no keys due to the method of winner generation. Winner will be chosen by modding a rand number by the LLL length of raffle objects.  
*/

#ifndef MUSICIANNODE_H
#define MUSICIANNODE_H

#include "musician.h"


class MusicianNode: public Musician
{
public:
	MusicianNode();
	~MusicianNode();

	MusicianNode * getNext();
	const int updateMusician(const Musician & musician);
	const int setNext(MusicianNode * & aNode);

	const int display() const;
	MusicianNode(const MusicianNode & aNode);
	MusicianNode& operator=(const MusicianNode & aNode);
protected:

	MusicianNode * next;
};

#endif

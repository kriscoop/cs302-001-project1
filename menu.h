#include "actor.h"
#include "artist.h"
#include "musician.h"
#include "raffle.h"
#include "actorNode.h"
#include "artistNode.h"
#include "ALL.h"
#include "CLL.h"
#include <algorithm>

#ifndef MENU_H
#define MENU_H

class Menu
{
public:
	Menu();
	Menu(const int & index);
	Menu(const int & index, const string & actor, const string & artist, const string & musician, const string & raffle);
	Menu(const int & index, istream & actor, istream & artist, istream & musician, istream & raffle);
	~Menu();
	
	int loadActor(istream & actorF);
	int loadArtist(istream & artistF);
	int loadMusician(istream & musicianF, istream & raffleF);

	int saveActor(ostream & actorF);
	int saveArtist(ostream & artistF);
	int saveMusician(ostream & musicianF, ostream & raffleF);
	int run();
	

private:
	ALL array;
	int arrayIndex;
	CLL list;
	vector<Musician> vec;

	int topMenuSelect(char & option);
	int actorMenuSelect(char & option);
	int artistMenuSelect(char & option);
	int musicianMenuSelect(char & option);

	int printTopMenu() const;
	int topMenuRead(char & option);
	int printActorMenu() const;
	int actorMenuRead(char & option);
	int printArtistMenu() const;
	int artistMenuRead(char & option);
	int printMusicianMenu() const;
	int musicianMenuRead(char & option);

	int displayActor();
	int insertActor();
	int retrieveActor(char & option);
	int removeActor();
	int destroyActor(char & option);
	int loadActor(char & option);
	int saveActor();

	int displayArtist();
	int insertArtist();
	int retrieveArtist(char & option);
	int removeArtist();
	int destroyArtist(char & option);
	int loadArtist(char & option);
	int saveArtist();
	
	int displayMusician();
	int insertMusician();
	int retrieveMusician(char & option);
	int removeMusician();
	int destroyMusician(char & option);
	int loadMusician(char & option);
	int saveMusician();

	int createActor(ActorNode*& actor) const;
	int manipActor(ActorNode*& actor, char & option);
	int createArtist(ArtistNode*& artist) const;
	int createRaffle(Raffle*& raffle) const;
	int manipArtist(ArtistNode*& artist, char & option);
	int createMusician(Musician*& musician) const;
	int manipMusician(Musician*& musician, char & option);

	int printActorManip() const;
	int printArtistManip() const;
	int printMusicianManip() const;
	int actorActions(ActorNode*& actor, char & option);
	int artistActions(ArtistNode*& artist, char & option);
	int musicianActions(Musician*& musician, char & option);
	
	int addSlot(ActorNode*&);
	int clearSlot(ActorNode*&);
	int fillSlot(ActorNode*&);
	int slotsFull(ActorNode*&);
	int updateStart(ActorNode*&);

	int purchaseRaffle(ArtistNode*&);
	int pickWinner(ArtistNode*&);
	int probability(ArtistNode*&);

	int updateOptions(Musician*&);
	int capIncrease(Musician*&);
	int calcRev(Musician*&);
	int purchaseTicket(Musician*&);
	int soldOut(Musician*&);
	
};

#endif

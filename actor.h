/*
Name: Kristian Cooper
Class#: CS-302-001
Project1
actor.h
This class is a derived class from the person class. It will have additional methods that allow user interaction such as checking if there are available slots, filling a slot, and clearing a slot. 
*/

#ifndef ACTOR_H
#define ACTOR_H

#include "person.h"
#include <cmath>

class Actor : public Person
{
	friend istream& operator>>(istream & stream, Actor & actor);
	friend ostream& operator<<(ostream & stream, Actor & actor);
public:
	Actor();
	Actor(istream & input);
	Actor(const string & aName, const int & section, const int & booth, const int & start,  const string & aString, const int & slotNum);
	~Actor();

	int slotsFull() const;
	int addSlots(const int & aNum);
	int updateStart(const int & aNum);
	int clearSlot(const int & aNum);
	int fillSlot(const int & aNum);

	int display() const;
	int displaySched() const;
	Actor(const Actor & to_copy);
	Actor & operator=(const Actor & to_copy);

protected:
	
	int startTime;
	char * promotion;
	int numSlots;
	int * slots;

};

#endif

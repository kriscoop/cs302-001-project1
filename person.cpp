#include "person.h"

Person :: Person(): name(nullptr), section(0), booth(0) 
{
}

Person :: Person(istream & input): name(nullptr), section(0), booth(0)
{
	input >> *this;
}

Person :: Person(const string & aName, const int & aSection, const int & aNum): name(nullptr), section(aSection), booth(aNum)
{
	copy(name,aName.c_str()); 
}

Person :: ~Person()
{
	if(name)
		delete [] name;
}

int Person :: updateName(const string & aName)
{
	return copy(name, aName.c_str());
}

int Person :: findName(const string & aName)
{
	if(strcmp(name, aName.c_str()) != 0)
		return 1;
	return 0;
}

int Person :: updateLocation(const int & aSection, const int & aNum)
{
	section = aSection;
	booth = aNum;
	return 1;
}

int Person :: findLocation(const int & aSection, const int & aNum)
{
	if(section == aSection && booth == aNum)
		return 1;
	return 0;
}

int Person :: getSection() const
{
	return section;
}

int Person :: display() const
{
	if(name)
	{
		cout << "Name: " << name << endl;
		cout << "Location: " << section << "-" << booth << endl;
		return 1;
	}
	else
	{
		cout << "Name: ERROR" << endl;
		cout << "Location: " << section << "-" << booth<< endl;
		return 0;
	}
}

Person :: Person(const Person & to_copy)
{
	name = nullptr;
	copy(name, to_copy.name);
	section = to_copy.section;
	booth = to_copy.booth;
}

Person & Person :: operator=(const Person & to_copy)
{
	if(this == &to_copy)
		return *this;
	copy(name, to_copy.name);
	section = to_copy.section;
	booth = to_copy.booth;
	return *this;
}

bool Person :: operator<(const Person & to_copy)
{
	int test = strcmp(name, to_copy.name);
	if(test < 0 || (test == 0 && (section <= to_copy.section && booth <= to_copy.booth)))
		return true;
	else
		return false;
}
	
int Person :: copy(char*& copy_to, const char * to_copy)
{
	if(copy_to)
		delete [] copy_to;
	if(to_copy)
	{
		copy_to  = new char[strlen(to_copy) + 1];
		strcpy(copy_to, to_copy);
		return 1;

	}
	else
		return 0;
}

istream& operator>> (istream& stream, Person & aPerson )
{
	char buffer[MAX_STRING];	
	stream.getline(buffer, MAX_STRING,';');

	if(aPerson.name)
		delete aPerson.name;
	aPerson.updateName(buffer);
	stream >> aPerson.section;
	stream.ignore(1, ';');
	stream >> aPerson.booth;
	stream.ignore(1, '\n');
	
	return stream;
}

ostream& operator<< (ostream & stream, Person & aPerson)
{
	stream << aPerson.name;
	stream << ';';
	stream << aPerson.section;
	stream << ';';
	stream << aPerson.booth;
	stream << '\n';

	return stream;
}


/*
Name: Kristian Cooper
Class#: CS-302-001
Project1
CLL.h This class is a derived class from the person class. It will have additional methods that allow user interaction such as checking if there are available slots, filling a slot, and clearing a slot. 
*/


#ifndef CLL_H
#define CLL_H


#include "actorNode.h"

class CLL
{
	friend istream & operator>> (istream & input, CLL & list);
	friend ostream & operator<< (ostream & input, CLL & list);
	
public:
	CLL();
	CLL(ActorNode * & aNode); 
	CLL(const ActorNode & artist);
	~CLL();

	int retrieveActor(const int & num, ActorNode *& aNode);
	int insertActor(ActorNode * & aNode);
	int display() const;
	int removeActor(const int & num);
	int removeAll();
	
	CLL (const CLL & list);
	CLL & operator=(const CLL & list);

private:
	ActorNode * rear;

	//Recursive Functions
	int retrieveActor(ActorNode * & current, const int & num, int & index, ActorNode *& aNode);
	int insertActor(ActorNode * & head, ActorNode * & aNode);
	int insertActor(ActorNode * & head, int & index, const int & section, ActorNode *& aNode);
	int destroy();
	void destroy(ActorNode * & head);
	void display(ActorNode * & head, int & index) const;
	void display(ActorNode * & head, int & index, int & position) const;
	int removeActor(ActorNode * & current, ActorNode *& prev, const int & num, int & position);
	ActorNode *& copy(ActorNode *& current, ActorNode *& rear);
	int printToFile(ActorNode *& current, ostream & stream) const;

};

#endif

#include "ALL.h" 

ALL :: ALL():size (0), head(nullptr)
{
}

ALL :: ALL(const int & MAX): size(MAX)
{
	head = new ArtistNode*[size];
	for(int i = 0; i<size;i++)
		head[i] = nullptr;
}

ALL :: ALL(ArtistNode * & aNode, const int & MAX): size(MAX), head(nullptr)
{
	head = new ArtistNode*[size];
	for(int i = 0; i<size;i++)
		head[i] = nullptr;

	ArtistNode * newNode = new ArtistNode(*aNode);
	if(insertArtist(newNode) == -1)
		cout << "Error, invalid section. Unable to add artist" << endl;
}

ALL :: ALL(const ArtistNode & artist, const int & MAX): size(MAX), head(nullptr)
{
	head = new ArtistNode*[size];
	for(int i = 0; i<size;i++)
		head[i] = nullptr;
	ArtistNode * newNode = new ArtistNode(artist);
	if(insertArtist(newNode) == -1)
		cout << "Error, invalid section. Unable to add artist" << endl;
}

ALL :: ~ALL()
{
	destroy();
}

int ALL :: retrieveArtist(const int & section, const int & num, ArtistNode * & aNode)
{
	if(!head || section -1 >= size || num <= 0)
		return -1;
	else
	{
		int index = 0;
		aNode = retrieveArtist(head[index], section, num, index);
		if(!aNode)
			return 0;
		else
			return 1;
	}
}

ArtistNode *& ALL :: retrieveArtist(ArtistNode * & head, const int & section, const int & num, int & index)
{
	if(section-1 == index)
	{
		if(!head)
			return head;
		else
		{
			int position = 0;
			ArtistNode ** result;
			result = &retrieveArtist(head, num, position);
			return *result;
		}
	}
	else
	{
		index++;
		if(index >= size)
			return this->head[index];
		else
			return retrieveArtist(this->head[index], section, num, index);
	}
}

ArtistNode *& ALL :: retrieveArtist(ArtistNode *& head, const int & num, int & position)
{
	if(!head)
		return head;
	else
	{
		if(position == num-1)
			return head;
		else
		{
			position++;
			return retrieveArtist(head->getNext(), num, position);
		}
	}
}

int ALL :: insertArtist(ArtistNode * & aNode)
{
	if(!head)
		return -1;
	else
	{
		int index = 0, section = aNode->getSection()-1;
		if(section >= size)
			return 0;
		else
			return insertArtist(head[index], index, section, aNode);
	}
}

int ALL :: insertArtist(ArtistNode * & head, int & index, const int & section, ArtistNode *& aNode)
{
	if(index < size)
	{
		if(index == section)
		{
			if(head)
			{
				if(*aNode < *head)
				{
					ArtistNode * newNode = new ArtistNode(aNode);
					newNode->setNext(head);
					this->head[index] = newNode;
					delete aNode;
					aNode = nullptr;
					return 1;
				}
				else
					return insertArtist(head, aNode);
			}
			else
			{
				ArtistNode * newNode = new ArtistNode(aNode);
				head = newNode;
				delete aNode;
				aNode = nullptr;
				return 1;
			}
		}
		else
		{
			index++;
			return insertArtist(this->head[index], index, section, aNode);
		}	
	}
	else
		return 0;
}

int ALL :: insertArtist(ArtistNode * & head, ArtistNode * & aNode)
{
	if(!head)
	{
		ArtistNode * newNode = new ArtistNode(aNode);
		head = newNode;
		delete aNode;
		aNode = nullptr;
		return 1;
	}
	else
	{
		if(!head->getNext())	
			return insertArtist(head->getNext(), aNode);	
		else
		{
			if(*head->getNext() < *aNode)
				return insertArtist(head->getNext(), aNode);
			else
			{
				ArtistNode * newNode = new ArtistNode(aNode);
				newNode->setNext(head->getNext());
				head->setNext(newNode);
				delete aNode;
				aNode = nullptr;
				return 1;
			}
		}
		
	}
}

int ALL :: display() const
{
	if(!head)
		return 0;
	else
	{
		int index = 0;
		display(head[index], index);
	}
	return 1;
}

void ALL :: display(ArtistNode *& head, int & index) const
{
	if(index < size)
	{
		cout << "Section: " << index +1 << endl; 
		int position = 1;
		display(head, index, position);
		index++;
		display(this->head[index], index);
	}
}

void ALL :: display(ArtistNode * & head, int & index, int & position) const
{
	if(head)
	{
		cout << index+1 << "." << position << endl;
		head->display();
		cout << endl;
		position++;
		display(head->getNext(), index, position);
	}
}

int ALL :: removeArtist(const int & section, const int & num)
{
	if(!head || section-1 >= size || num <= 0)
		return -1;
	else
	{
		int index = 0;
		return removeArtist(head[index], section, num, index);
	}
}

int ALL :: removeArtist(ArtistNode * & head, const int & section, const int & num, int & index)
{
	if(index < size)
	{
		if(section-1 == index)
		{
			if(num-1 == 0)
			{
				ArtistNode * deletion = head;
				this->head[index] = head->getNext();
				delete deletion;
				return 1;
			}
			else
			{
				int position = 1;
				return removeArtist(head->getNext(), head, num, position); 
			}
		}
		else
		{
			index++;
			return removeArtist(this->head[index], section, num, index);
		}
	}
	else
		return 0;
}

int ALL :: removeArtist(ArtistNode * & current, ArtistNode * & prev, const int & num, int & position)
{
	if(!current)
		return 0;
	else
	{
		if(position == num - 1)
		{
			ArtistNode * deletion = current; 
			prev->setNext(current->getNext());
			delete deletion;
			return 1;
		}
		else
		{
			position++;
			return removeArtist(current->getNext(), current, num, position); 
		}
	}
}			

int ALL :: removeAll()
{
	int result = destroy();
	return result;
}

int ALL :: destroy()
{
	int index = 0;
	if(head)
	{
		destroy(head[index], index);
		delete [] head;
		head = nullptr;
		return 1;
	}
	return 0;
}

void ALL :: destroy(ArtistNode * & head, int & index)
{
	if(index < size)
	{
		if(head)
			destroy(head);
		index++;
		destroy(this->head[index], index);
	}
}

void ALL :: destroy(ArtistNode * & head)
{
	if(head)
	{
		destroy(head->getNext());
		delete head;
	}
}

ALL :: ALL (const ALL & to_copy): head(nullptr)
{
	size = to_copy.size;
	
	if(to_copy.head)
	{
		head = new ArtistNode*[size];
	
		int index = 0;

		ArtistNode ** current = to_copy.head;
		copy(current, index);
	}
}

ALL & ALL :: operator= (const ALL & to_copy)
{
	if(this == &to_copy)
		return *this;

	size = to_copy.size;

	if(head)
		destroy();
	
	if(to_copy.head)
	{
		head = new ArtistNode*[size];
		ArtistNode ** current = to_copy.head;

		int index = 0;
		copy(current, index);
	}

	return *this;
}

int ALL :: copy(ArtistNode ** to_copy, int & index)
{
	if(index < size)
	{
		if(to_copy[index])
		{
			ArtistNode * newNode  = new ArtistNode(*to_copy[index]);
			head[index] = newNode;
			newNode->setNext(copy(to_copy[index]->getNext()));
		}
		else
			head[index] = nullptr;
		index++;
		return copy(to_copy, index);
	}
	else
		return 1;
}

ArtistNode*& ALL :: copy(ArtistNode *& current)
{
	if(!current)
		return current;
	else
	{
		ArtistNode * newNode = new ArtistNode(*current);
		newNode->setNext(copy(current->getNext()));
		return newNode;
	}
}

istream & operator>>(istream & stream, ALL & list)
{
	if(stream)
	{
		if(list.head)
			list.destroy();
		stream >> list.size;
		stream.ignore(1,';');
		list.head = new ArtistNode*[list.size];
		for(int i =0; i<list.size;i++)
			list.head[i] = nullptr;
		ArtistNode * artist = new ArtistNode(stream);

		while(!stream.eof())
		{	
			list.insertArtist(artist);
			delete artist;
			artist = nullptr;
			artist = new ArtistNode(stream);
		}
		delete artist;
	}
	return stream;
}

ostream & operator<<(ostream & stream, ALL & list)
{
	if(stream)
	{
		if(list.head)
		{
			int index = 0;
			int size = list.size;
			list.printToFile(list.head, size, index, stream);
		}
	}
	return stream;
}

int ALL :: printToFile(ArtistNode **& head, const int & size, int & index, ostream & stream)
{
	if(index < size)
	{
		if(head[index])
		{
			printToFile(head[index], stream);
			index++;
			return printToFile(head, size, index, stream);
		}
		else
		{
			index++;
			return printToFile(head, size, index, stream);
		}
	}
	return 1;
}

int ALL :: printToFile(ArtistNode *& current, ostream& stream)
{
	if(!current)
		return 1;
	else
	{
		stream << current;
		return printToFile(current->getNext(), stream);
	}
}




